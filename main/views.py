from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Matkul



def home(request):
    return render(request, 'main/home.html')
def Experience(request):
    return render(request, 'main/Experience.html')
def Story1(request):
    return render(request, 'main/Story1.html')
def story5(request):
    return render(request, 'main/story5.html')


def masuk(request):
    matkul = Matkul.objects.all()
    html = 'main/story5.html'
    context = {'matkul':matkul}
    return render(request, html, context)

def addMatkul(request):
    c = request.POST['nama']
    new_item = Matkul(nama = request.POST['nama'])
    new_item.save()
    return HttpResponseRedirect('/story5/')

def addDosen(request):
    c = request.POST['dosen']
    new_item = Matkul(nama = request.POST['dosen'])
    new_item.save()
    return HttpResponseRedirect('/story5/')

def addSKS(request):
    c = request.POST['sks']
    new_item = Matkul(nama = request.POST['sks'])
    new_item.save()
    return HttpResponseRedirect('/story5/')

def deleteMatkul(request, context_id):
    item_to_delete = Matkul.objects.get(id=context_id)
    item_to_delete.delete()
    return HttpResponseRedirect('/story5/')

