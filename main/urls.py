from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('home.html', views.home, name='home'),
    path('Experience.html', views.Experience, name='Experience'),
    path('Story1.html', views.Story1, name='Story1'),
    path('story5.html',views.masuk, name='story5'),
    path('story5.html',views.addMatkul, name='story5'),

]
